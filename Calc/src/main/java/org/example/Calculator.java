package org.example;

import java.util.Arrays;
import java.util.Scanner;

public class Calculator {
    private double num1,num2;

    public static double add(double num1,double num2){
        return num1+num2;
    }
    public static double minus(double num1,double num2){
        return num1-num2;
    }
    public static double multiply(double num1,double num2){
        return  num1*num2;
    }
    public static double divide(double num1, double num2){
        return num2==0? 0: num1/num2;
    }
    public static double percentagem(double num1, double num2){
        return num2==0? 0: num1%num2;
    }

    public static void main(String[] args) {
        String conta;
        String splitted[]=new String[3];
        double result;
        Scanner sc=new Scanner(System.in);
       while (true){
           System.out.println("Calculator");
           conta=sc.nextLine();
           conta=conta.replaceAll("\\s","");
           System.out.println("A conta e "+ conta);
           splitted= conta.split("");


          result= switch (splitted[1]){
               case "+"-> add(Double.parseDouble(splitted[0]),Double.parseDouble(splitted[2]));
               case "-"-> minus(Double.parseDouble(splitted[0]),Double.parseDouble(splitted[2]));
               case "*"-> multiply(Double.parseDouble(splitted[0]),Double.parseDouble(splitted[2]));
               case "/"-> divide(Double.parseDouble(splitted[0]),Double.parseDouble(splitted[2]));
              default -> 0.0;
           };
           System.out.println("O resultado e :"+result);

       }
    }
}